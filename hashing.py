#!/usr/bin/env python

import hashlib


def hashing_method(word, hash_method):
    if hash_method == "md5":
        out_put = hashlib.md5(word).hexdigest()
        return out_put
    elif hash_method == "sha1":
        out_put = hashlib.sha1(word).hexdigest()
        return out_put
    elif hash_method == "sha224":
        out_put = hashlib.sha224(word).hexdigest()
        return out_put
    elif hash_method == "sha256":
        out_put = hashlib.sha256(word).hexdigest()
        return out_put
    elif hash_method == "sha512":
        out_put = hashlib.sha512(word).hexdigest()
        return out_put
    elif hash_method == "sha384":
        out_put = hashlib.sha384(word).hexdigest()
        return out_put
    else:
        return False

def num_to_method(num):
    if num == "1":
        method = "md5"
    elif num == "2":
        method = "sha1"
    elif num == "3":
        method = "sha224"
    elif num == "4":
        method = "sha256"
    elif num == "5":
        method = "sha384"
    elif num == "6":
        method = "sha512"
    else:
        method = "[-] Not found!"

    return method

def main():
        word = raw_input("\n[?] Enter your word: ")
        print('\n[1] md5 \n[2] sha1 \n[3] sha224 \n[4] sha256 \n[5] sha384 \n[6] sha512 \n')
        method = raw_input("[?] Chose method of hash: ")
        method = num_to_method(method)

        if method == "[-] Not found!":
            print(method)
        else:
            print("\n"+'[+] Your hash: ' + hashing_method(word, method))

if __name__ == "__main__":
    main()
